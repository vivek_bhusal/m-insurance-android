package com.minsurance.nepal;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.minsurance.nepal.library.SharePrefManager;
import com.minsurance.nepal.library.UserFunctions;

public class Makeenquiry extends Activity{
	String token;
	String query;
	EditText question;
	SharePrefManager SM;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.askquestion);
		SM = new SharePrefManager(Makeenquiry.this);
		HashMap<String, String> getuserdetails = new HashMap<String, String>();
		getuserdetails = SM.getuserDetails();
		token = getuserdetails.get(SharePrefManager.KEY_TOKENID);
		Log.i("token again", token);
		
		question = (EditText) findViewById(R.id.ET_question);
		
		TextView makequery = (TextView) findViewById(R.id.makequery);
		makequery.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				query = question.getText().toString();
				
				if(query.length()<1){
					Toast.makeText(getApplicationContext(), "Please enter your query", Toast.LENGTH_LONG).show();
				}else{
					AsynConnection connection = new AsynConnection();
					connection.execute();
				}
				
			}
		});
	}
	
	private class AsynConnection extends AsyncTask<Void, Void, Void>{
		JSONObject jsob;
		@Override
		protected Void doInBackground(Void... params) {
			UserFunctions user = new UserFunctions();
			jsob = user.makeenquiry(token, query);
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			try {
				if(jsob.getString("status").equalsIgnoreCase("Success")){
					Toast.makeText(getApplicationContext(), jsob.getString("message"), Toast.LENGTH_LONG).show();
					Intent intent = new Intent(Makeenquiry.this, Dashboard.class);
					startActivity(intent);
				}else{
					
					Log.i("json", jsob.toString());
					Toast.makeText(getApplicationContext(), jsob.toString(), Toast.LENGTH_LONG).show();
					
					SM.putuserDetails("0", null);
					Toast.makeText(getApplicationContext(), "Authentication Error", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(Makeenquiry.this, MainActivity.class);
					startActivity(intent);					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}

}
