package com.minsurance.nepal;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.minsurance.nepal.library.SharePrefManager;
import com.minsurance.nepal.library.UserFunctions;
import com.minsurance.nepal.library.checkconnection;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	EditText email, password;
	String STR_email, STR_pass;
	SharePrefManager SM;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		SharePrefManager SM = new SharePrefManager(MainActivity.this);
		HashMap<String, String> getuserdetails = new HashMap<String, String>();
		getuserdetails = SM.getuserDetails();
		String islogin = getuserdetails.get(SharePrefManager.KEY_ISLOGEDIN);
		
		if(islogin.equalsIgnoreCase("1")){
			Intent intent = new Intent(MainActivity.this, Dashboard.class);
			intent.putExtra("notification", "0");
			startActivity(intent);
		}
		
		
		email = (EditText) findViewById(R.id.useremail);
		password = (EditText) findViewById(R.id.password);
		if(checkconnection.checkInternetConnection(MainActivity.this)){
			Button login = (Button) findViewById(R.id.login);
			login.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					STR_email = email.getText().toString();
					STR_pass = password.getText().toString();
					if(STR_email.length()<1 || STR_pass.length()<1){
						Toast.makeText(getApplicationContext(), "Please Enter All Details", Toast.LENGTH_LONG).show();
					}else{
						AsyncConncetion connection = new AsyncConncetion();
						connection.execute();
					}
				}
			});
		}else{
			Toast.makeText(getApplicationContext(), "No Internet Availible", Toast.LENGTH_LONG).show();
		}
	}
	
	private class AsyncConncetion extends AsyncTask<Void, Void, Void>{

		JSONObject jsonOb;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		@Override
		protected Void doInBackground(Void... arg0) {
			UserFunctions user = new UserFunctions();
			jsonOb = user.login(STR_email, STR_pass);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			try {
				if(jsonOb.getString("status").equalsIgnoreCase("success")){
					SM = new SharePrefManager(getApplicationContext());
					JSONObject tempObject = jsonOb.getJSONObject("data");
					Log.i("token", tempObject.getString("token"));
						SM.putuserDetails("1", tempObject.getString("token"));
						Intent intent = new Intent(MainActivity.this, Dashboard.class);
						intent.putExtra("notification", tempObject.getString("notifications"));
						startActivity(intent);
											
				}else{
					Toast.makeText(getApplicationContext(), jsonOb.getString("message"), Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
	}
	


}
