package com.minsurance.nepal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Dashboard extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		Bundle extra = getIntent().getExtras();
		if(extra !=null){
			String notification = extra.getString("notification");
			if(!notification.equalsIgnoreCase("0")){
				
				TextView number_notification = (TextView) findViewById(R.id.number_notification);
				number_notification.setText(notification);	
				number_notification.setVisibility(View.VISIBLE);
			}
					
		}
		
		LinearLayout policy = (LinearLayout) findViewById(R.id.mypolicy_layout);
		LinearLayout claim = (LinearLayout) findViewById(R.id.myclaim_layout);
		LinearLayout enquiry = (LinearLayout) findViewById(R.id.enquiry_layout);
		LinearLayout notification = (LinearLayout) findViewById(R.id.notification_layout);
		
		policy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(Dashboard.this, Mypolicy.class);
				startActivity(intent);
				
			}
		});
		
		claim.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Dashboard.this, Claim.class);
				startActivity(intent);
				
			}
		});
		
		enquiry.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Dashboard.this, Makeenquiry.class);
				startActivity(intent);
			}
		});
		
		notification.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Dashboard.this, Notification.class);
				startActivity(intent);
			}
		});
		
	}

}
