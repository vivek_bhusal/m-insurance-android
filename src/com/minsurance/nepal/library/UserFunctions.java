package com.minsurance.nepal.library;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;


public class UserFunctions {
	
	private JSONParser jsonParser;
	private static String BaseURL = "http://http://minsurancenepal.com/";
	private static String loginURL = "http://minsurancenepal.com/minsurance/public/default/service/login";
	private static String getpolicyURL = "http://minsurancenepal.com/minsurance/public/default/service/get-policy-list";
	private static String getnotificationurl = "http://minsurancenepal.com/minsurance/public/default/service/get-notifications";
	private static String makequeryurl = "http://minsurancenepal.com/minsurance/public/default/service/make-enquiry";
	
	private static int PUT = 1;
	private static int GET = 2;
	private static int POST = 3;
	
	JSONObject jsonobject = null;
	
	// constructor
	public UserFunctions(){
		jsonParser = new JSONParser();
	}
	
	public JSONObject login(String username, String password){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("password", password));
		try{
			jsonobject = jsonParser.getJSONFromUrl(loginURL, params, POST);
		}catch (URISyntaxException e){
			e.printStackTrace();
		}
		return jsonobject;
	}
	
	public JSONObject getpolicy(String token){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("token", token));
		try{
			jsonobject = jsonParser.getJSONFromUrl(getpolicyURL, params, POST);
		}catch (URISyntaxException e){
			e.printStackTrace();
		}
		return jsonobject;
	}
	
	public JSONObject getnotification(String token){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("token",token));
		try{
			jsonobject = jsonParser.getJSONFromUrl(getnotificationurl, params, POST);
		}catch(URISyntaxException e){
			e.printStackTrace();
		}
		return jsonobject;
	}
	
	public JSONObject makeenquiry(String token, String query){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("token",token));
		params.add(new BasicNameValuePair("customer_query",query));
		try{
			jsonobject = jsonParser.getJSONFromUrl(makequeryurl, params, GET);
		}catch(URISyntaxException e){
			e.printStackTrace();
		}
		return jsonobject;
	}
		
}
